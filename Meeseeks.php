<?php
/**
 * Curse Inc.
 * Meeseeks
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2016 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Meeseeks
 * @link		https://gitlab.com/hydrawiki
 *
**/

if (function_exists('wfLoadExtension')) {
	wfLoadExtension('Meeseeks');
	wfWarn(
		'Deprecated PHP entry point used for Meeseeks extension. Please use wfLoadExtension instead, ' .
		'see https://www.mediawiki.org/wiki/Extension_registration for more details.'
	);
	return;
} else {
	die('This version of the Meeseeks extension requires MediaWiki 1.25+');
}
