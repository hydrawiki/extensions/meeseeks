<?php
/**
 * Curse Inc.
 * Meeseeks
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2016 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Meeseeks
 * @link		https://gitlab.com/hydrawiki
 *
**/

class MeeseeksHooks {
	/**
	 * Add parser hood for Meeseeks tag
	 * @param  Parser $parser
	 * @return nothing
	 */
	static function onParserFirstCallInit( Parser $parser ) {
		global $wgOut;
		$parser->setHook( 'meeseeks', 'MeeseeksHooks::renderTag' );
		$wgOut->addModules( 'ext.meeseeks' );
 	}

	/**
	 *  Handle the parsing of the meeseeks tag
	 * @param  $input
	 * @param  array   $args
	 * @param  Parser  $parser
	 * @param  PPFrame $frame
	 * @return string
	 */
 	static function renderTag( $input, array $args, Parser $parser, PPFrame $frame ) {
 		if (isset($args['chance']) && is_numeric($args['chance'])) {
			$chance = $args['chance'];
		} else {
			$chance = 100;
		}
		return "<script type=\"text/javascript\">meeseeksChance = ".$chance.";</script>";
 	}
}
